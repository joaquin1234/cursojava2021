package pruebasDB;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class PruebasDB {
	//Marca de la base de datos, mysql, oracle, mssql, db2, etc...
	static String dbbrand = "mysql";
	//nombre del servidor donde se ejecuta la base de datos, si es mi pc puede ser localhost o 127.0.0.1
	static String host = "localhost";
	//port de la base de datos, mysql3306, db2 50000, oracle 1521
	static String port = "3306";
	//nombre de la base de datos (o schema en oracle y mysql) al que me voy a conectar
	static String dbname = "codigos_postales";
	//otros argumentos de conexi�n, estos son para mysql 8
	static String conargs = "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	//aca armo la cadena de conexion que le voy a pasar al driver manager
	static String constr = "jdbc:" + dbbrand + "://" + host + ":" + port + "/" + dbname + conargs;
	//usuario y pwd de conexion a la base de datos, esto estaria bueno que no este como texto plano
	static String usrdb = "root";
	static String pwddb = "rootroot";
	//nombre de la clase de la base de datos
	static String dbclass = "com.mysql.cj.jdbc.Driver";

	public static void main(String[] args) {
		

		try {
			// 1 - Driver manager instancio el driver
			Class.forName(dbclass);
			try {
				// 2 - Creo la conexion a la base de datos
				Connection con = DriverManager.getConnection(constr, usrdb, pwddb);
				System.out.println("Me conecte a la base");
				// 3 - Creo un Statement
				Statement stm = con.createStatement();
				
				//4 - ejecuto un query y lo asigno a un resultset
				ResultSet rs = stm.executeQuery("Select * from codigos_postales.provincias");
				
				//imprimo la cantidad de registros
				
				
				while (rs.next()) {
					System.out.println(rs.getString("nombre"));
				};
				
			
			
			} catch (SQLException e) {
				System.out.println("no me pude conectar a la base");
				e.printStackTrace();
			}
			System.out.println("pude instanciar la clase driver");
		} catch (ClassNotFoundException e) {
			System.out.println("no pude instanciar el driver");
			e.printStackTrace();
		}

	}
}
