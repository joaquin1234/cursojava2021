package util;

import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;




public class DateUtil {

	public static int getAnio (Date fecha) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		return cal.get(Calendar.YEAR);
	}
	
public static int getMes (Date fecha) {
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		return cal.get(Calendar.MONTH)+1;
	}

public static int getDia (Date fecha) {
	
	Calendar cal = Calendar.getInstance();
	cal.setTime(fecha);
	return cal.get(Calendar.DATE);
	}


//static Date asDate(String pattern, String fecha)
public static Date asDate(String pattern, String fecha) {
	
	
	Date fecha_formateada = null;
	
	try {
		
		
		fecha_formateada = new SimpleDateFormat(pattern).parse(fecha);
		
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return fecha_formateada;
	}


public static boolean esFinde (Date fecha) {
	
	Calendar cal = Calendar.getInstance();
	
	cal.setTime(fecha);
	
	 return (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY );
	}


}
