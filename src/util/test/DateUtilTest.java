package util.test;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.DateUtil;


public class DateUtilTest {

	Date fechaCumple;
	
	
	@Before
	public void setUp() throws Exception {
		Calendar cal = Calendar.getInstance();
		cal.set(1978, Calendar.JULY, 5);
		fechaCumple = cal.getTime();
		System.out.println(cal.get(Calendar.DAY_OF_WEEK));
	}

	@After
	public void tearDown() throws Exception {
		fechaCumple = null;
		
	}

	@Test
	public void testGetAnio() {
		
		assertEquals(1978, DateUtil.getAnio(fechaCumple));
	}
	
	@Test
	public void testGetMes() {
		
		assertEquals(7, DateUtil.getMes(fechaCumple));
	}

	@Test
	public void testGetDia() {
		
		assertEquals(5, DateUtil.getDia(fechaCumple));
	}
	
	@Test
	public void testEsFinde() {
		
		assertEquals(false, DateUtil.esFinde(fechaCumple));
	}
	
	@Test
	public void testAsDate() {
		
		assertEquals(fechaCumple, DateUtil.asDate("yyyyMMdd","19780705"));
	}
}
