package modulo3;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Ejercicio3_pantalla_switch {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ejercicio3_pantalla_switch window = new Ejercicio3_pantalla_switch();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Ejercicio3_pantalla_switch() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 271, 160);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(21, 23, 222, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(21, 91, 222, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton = new JButton("Calcula");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String mes = textField.getText() ;
				String cantDias = "";
				switch (Integer.parseInt(textField.getText())) {
					case  1,3,5,7,8,10,12:
						cantDias = "31 d�as";
					break;
					case  2:
						cantDias = "28 d�as";
						break;
					case 4 ,6 , 9 , 11:
						cantDias = "30 d�as";
					break;
					default: cantDias = "mes invalido";
				
				};
				textField_1.setText(cantDias);
				
				
			}
		});
		btnNewButton.setBounds(21, 57, 222, 23);
		frame.getContentPane().add(btnNewButton);
	}
}
