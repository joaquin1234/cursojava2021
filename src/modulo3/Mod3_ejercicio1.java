package modulo3;

import java.util.Scanner;

public class Mod3_ejercicio1 {

	public static void main(String[] args) {
		// TODO promedio aprobado  o reprobado
		System.out.println("Ingrese nota 1");
		Scanner scnota1 = new Scanner(System.in);
				
		double nota1 = scnota1.nextDouble();
		
		System.out.println("Ingrese nota 2");
		Scanner scnota2 = new Scanner(System.in);
		double nota2 = scnota2.nextDouble();
		
		System.out.println("Ingrese nota 3");
		Scanner scnota3 = new Scanner(System.in);
		double nota3 = scnota3.nextDouble();
		
		double promedio = (nota1 + nota2 + nota3)/3;
		
		
		if (promedio < 7) {
			System.out.println("Promedio: " + promedio + " Reprobado");
		} else {
			
			System.out.println("Promedio: " + promedio + " Aprobado");
		}
		scnota1.close();
		scnota2.close();
		scnota3.close();

	}

}
