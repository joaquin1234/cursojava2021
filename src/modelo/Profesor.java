package modelo;

import java.util.Objects;

public class Profesor extends Persona {
	
	private String iosfa;
	
	//constructors
	
	public Profesor() { super();}
	
	public Profesor(String p_nombre, String p_apellido, String p_iosfa) {
		super(p_nombre, p_apellido);
		iosfa=p_iosfa;
	}
	
	
	//setters
	
		public void setIosfa (String p_iosfa) {
			
			iosfa = p_iosfa;

		}
		
	//getters
		
		public String getIosfa () {
			
			return iosfa ;

		}
		
	//otros
	    
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = super.hashCode();
			result = prime * result + Objects.hash(iosfa);
			return result;
		}
	
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (!super.equals(obj))
				return false;
			if (!(obj instanceof Profesor))
				return false;
			Profesor other = (Profesor) obj;
			return Objects.equals(iosfa, other.iosfa);
		}
	
		@Override
		public String toString() {
			return "Profesor [iosfa=" + iosfa + ", getIosfa()=" + getIosfa() + ", hashCode()=" + hashCode()
					+ ", getApellido()=" + getApellido() + ", getNombre()=" + getNombre() + ", toString()="
					+ super.toString() + ", getClass()=" + getClass() + "]";
		}

}
