package modelo.test;

import static org.junit.Assert.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.swing.plaf.synth.SynthSeparatorUI;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import modelo.Alumno;
import modelo.Persona;
import modelo.Profesor;

public class PersonaTest {
	List<Persona> lstPersonas;
	Persona per1;
	Alumno alu1;
	Profesor pro1;
	
	@Before
	public void setUp() throws Exception {
		per1 = new Persona("Joaquin", "Corres");
		alu1 = new Alumno ("Juanma", "Corresssss", 1234);
		pro1 = new Profesor("Gabriel", "Casas", "nose");
		
		
		lstPersonas = new ArrayList<>();
		lstPersonas.add(new Alumno("Joaquin", "CORRES", 1));
		lstPersonas.add(new Alumno("Sofia", "Romero", 2));
		lstPersonas.add(new Alumno("Luis", "Riquelme", 3));
		lstPersonas.add(new Alumno("Diana", "Mart", 4));
		lstPersonas.add(new Alumno("Sole", "Cejas", 5));
		lstPersonas.add(new Alumno());
		
		lstPersonas.add(new Profesor("YOSI", "_YOSELINNN", "ABC"));
		lstPersonas.add(new Profesor("VIC", "VICTOR", "asdfww"));
		lstPersonas.add(new Profesor("Abel", "CURU", "sftdsavcv"));
		lstPersonas.add(new Profesor());

		
		
	}

	@After
	public void tearDown() throws Exception {
		lstPersonas = null;
		alu1 = null;
		per1 = null;
		pro1=null;
		
		
	}
	
	@Test
	public void testListaContains_true() {
		
	}

	@Test
	public void getPErsonatest() {
		assertEquals("Corres", per1.getApellido());
	}
	
	@Test
	public void getNombretest() {
		assertEquals("Joaquin", per1.getNombre());
	}
	
	@Test
	public void getAlumnoNombretest() {
		assertEquals("Juanma", alu1.getNombre());
	}
	
	@Test
	public void getAlumnoApellidotest() {
		assertEquals("Corresssss", alu1.getApellido());
	}

	@Test
	public void getAlumnoLegajotest() {
		assertEquals(1234, alu1.getLegajo());
	}


}
