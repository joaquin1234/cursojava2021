package modelo;

import java.util.Objects;

public class Persona {
	
	private String nombre;
	private String apellido;

	//constructors
		public Persona () {}
		
		public Persona (String p_nombre, String p_apellido) {
				nombre= p_nombre;
				apellido=p_apellido;
		}
		
		
		// setters
		public void setApellido (String p_apellido){
			this.apellido=p_apellido;
		}
		
		public void setNombre (String p_nombre) {
			nombre = p_nombre;
						
		}
	   
		
		//getters
		
		public  String getApellido() {
			
			return apellido;
		}
		
		public String getNombre() {
			
			return nombre;
		}
		
		
		//otros
	    

	
	@Override
		public int hashCode() {
			return Objects.hash(apellido, nombre);
		}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Persona))
			return false;
		Persona other = (Persona) obj;
		return Objects.equals(apellido, other.apellido) && Objects.equals(nombre, other.nombre);
	}
	
	
	
	
	
	public String toString() {
		return this.toString();
	}
	
}
