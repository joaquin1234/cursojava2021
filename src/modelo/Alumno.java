package modelo;

import java.util.Objects;

public class Alumno extends Persona {
	
	//attributes
	private int legajo;
	
	
	//constructors
	public Alumno () { super(); }
	
	public Alumno (String p_nombre, String p_apellido, int p_legajo) {
		super(p_nombre, p_apellido);
		legajo =p_legajo;
	}
	

	//setters
	
	public void setLegajo (int p_legajo) {
		
		legajo = p_legajo;

	}
	
	//getters
	
	public int getLegajo () {
		
		return legajo ;

	}
	

	//otros
    
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(legajo);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Alumno))
			return false;
		Alumno other = (Alumno) obj;
		return legajo == other.legajo;
	}

	@Override
	public String toString() {
		return "Alumno [legajo=" + legajo + ", getLegajo()=" + getLegajo() + ", hashCode()=" + hashCode()
				+ ", getApellido()=" + getApellido() + ", getNombre()=" + getNombre() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + "]";
	}
		
		
}
	
