package domicilios;

public class Domicilio
{
	
	private String provincia;
	private String ciudad;
	private String localidad;
	private String cp1974;
	private String cpa;
	private String calle;
	private int altura;
	/**
	 * @param provincia
	 * @param ciudad
	 * @param localidad
	 * @param cp1974
	 * @param cpa
	 * @param calle
	 * @param altura
	 */
	
	public Domicilio(){};
	
	public Domicilio(String provincia,String ciudad,String localidad,String cp1974,String cpa,String calle,int altura)
	{
		super();
		this.provincia=provincia;
		this.ciudad=ciudad;
		this.localidad=localidad;
		this.cp1974=cp1974;
		this.cpa=cpa;
		this.calle=calle;
		this.altura=altura;
	}

	public String getProvincia()
	{
		return provincia;
	}

	public void setProvincia(String provincia)
	{
		this.provincia=provincia;
	}

	public String getCiudad()
	{
		return ciudad;
	}

	public void setCiudad(String ciudad)
	{
		this.ciudad=ciudad;
	}

	public String getLocalidad()
	{
		return localidad;
	}

	public void setLocalidad(String localidad)
	{
		this.localidad=localidad;
	}

	public String getCp1974()
	{
		return cp1974;
	}

	public void setCp1974(String cp1974)
	{
		this.cp1974=cp1974;
	}

	public String getCpa()
	{
		return cpa;
	}

	public void setCpa(String cpa)
	{
		this.cpa=cpa;
	}

	public String getCalle()
	{
		return calle;
	}

	public void setCalle(String calle)
	{
		this.calle=calle;
	}

	public int getAltura()
	{
		return altura;
	}

	public void setAltura(int altura)
	{
		this.altura=altura;
	}
	
	public String toJson()
	{
		StringBuilder st = new StringBuilder();
		
		st.append("{\"provincia\"" + ":" + "\"" + provincia.toUpperCase() + "\",");
		st.append("\"ciudad\"" + ":" + "\"" + ciudad.toUpperCase() + "\",") ;
		st.append("\"localidad\"" + ":" + "\"" + localidad.toUpperCase() + "\",") ;
		st.append("\"cp1974\"" + ":" + "\"" + cp1974.toUpperCase() + "\",") ;
		st.append("\"cpa\"" + ":" + "\"" + cpa.toUpperCase() + "\",") ;
		st.append("\"calle\"" + ":" + "\"" + calle.toUpperCase() + "\",") ;
		st.append("\"altura\"" + ":" + "\"" + altura + "\"}") ;
		
		return st.toString();
	}

	@Override
	public String toString()
	{
		return "Domicilio [provincia="+provincia+", ciudad="+ciudad+", localidad="+localidad+", cp1974="+cp1974+", cpa="+cpa+", calle="+calle+", altura="+altura+"]";
	}

	
	

}
