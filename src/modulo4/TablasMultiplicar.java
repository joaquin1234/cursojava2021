package modulo4;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.InputMethodListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.JList;
import javax.swing.AbstractListModel;

public class TablasMultiplicar {

	private JFrame frame;
	private JTable table;
	private JTable table_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TablasMultiplicar window = new TablasMultiplicar();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TablasMultiplicar() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		String strTablas[] = new String[16];
		String strTabRes[] [] = new String [16] [2];
		
		for(int i =0;i<strTablas.length;i++)
			strTablas[i] = Integer.toString(i);
		JComboBox comboBox = new JComboBox();
		
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				for(int h=0; h<16; h++) {
					
					//for (int j=0; j<2; j++) {
						int res = 0 ;
						int sel = Integer.parseInt(comboBox.getSelectedItem().toString()) ;
					
						res = sel * h;
						
					strTabRes[h] [0] = comboBox.getSelectedItem() + "x" + Integer.toString(h); 
					strTabRes[h] [1] = Integer.toString(res);
					System.out.println(strTabRes [h] [0] + " = " + strTabRes [h] [1]);
												}
						
					
					
					}
					;
						
		
			
		});
		
		comboBox.setModel(new DefaultComboBoxModel(strTablas));   
		comboBox.setBounds(10, 11, 171, 22);
		frame.getContentPane().add(comboBox);
		
		JList list = new JList();
		list.setModel(new AbstractListModel() {
			String[] values = new String[] {"hola"};
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		list.setBounds(10, 58, 179, 192);
		frame.getContentPane().add(list);
		
		
		
		
		
	}
}
