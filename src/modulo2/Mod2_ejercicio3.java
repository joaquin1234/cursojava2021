package modulo2;
import java.util.Scanner;
public class Mod2_ejercicio3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("Ingrese los goles");
		Scanner scgoles = new Scanner(System.in);
		byte goles = scgoles.nextByte();
		
		System.out.println("Ingrese capacidad");
		Scanner sccapacidad = new Scanner(System.in);
		int capacidad = sccapacidad.nextInt();
		
		System.out.println("Ingrese promedio de gol");
		Scanner scpromgol = new Scanner(System.in);
		double promedio_goles = scpromgol.nextDouble();
		
		System.out.println("Ingrese Division");
		Scanner scdivision = new Scanner(System.in);
		String division = scdivision.next();
		
		System.out.println("\ngoles: " + goles );
		System.out.println("\ncapacidad: " + capacidad );
		System.out.println("\npromedio_goles: " + promedio_goles );
		System.out.println("\ndivision: " + division );
		
		
		// TODO Completar con el tipo de dato que corresponda, se debe tener en cuenta que la suma de 2 bytes
		//va a sobrepasar el byte.
		byte b=10;
		short s=20;
		int i = 30;
		long l= 40;
		int sumabb = b+b;
		System.out.println("\nsumabb: " + sumabb );
		int sumabs=b+s;
		System.out.println("\nsumabs: " + sumabs );
		long sumabi=b+i;
		System.out.println("\nsumabi: " + sumabi );
		long sumaii=i+i;
		System.out.println("\nsumaii: " + sumaii );
		long sumasl=s+l;
		System.out.println("\nsumasl: " + sumasl );

		scgoles.close();
		sccapacidad.close();
		scpromgol.close();
		scdivision.close();
		
	}
}
