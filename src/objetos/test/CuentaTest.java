package objetos.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import objetos.Cuenta;

public class CuentaTest {
	Cuenta CuentaPrueba1;
	Cuenta CuentaPrueba2;

	@Before
	public void setUp() throws Exception {
		int numeroCuenta1 = 10;
		double saldoCuenta1 = 1000.20;
		int numeroCuenta2 = 11;
		double saldoCuenta2 = 0.0;
		CuentaPrueba1 = new Cuenta(numeroCuenta1, saldoCuenta1);
		CuentaPrueba2 = new Cuenta(numeroCuenta2, saldoCuenta2);
	}
	
	@SuppressWarnings("deprecation")
	@Test
	public void testGetSaldo() {
		assertEquals(1000, CuentaPrueba1.getSaldo(10), 0.20);
	}




	@Test
	public void testDepositar() {
		CuentaPrueba2.depositar(11, 1000);
		assertEquals(1000, CuentaPrueba2.getSaldo(11), 0.20);
		
	}
	
	
	@After
	public void tearDown() throws Exception {
		CuentaPrueba1 = null;
		CuentaPrueba1 = null;
	}

}
