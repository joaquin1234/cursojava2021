package objetos;

public class Cuenta {

	
	private static int numero;
	private static double saldo = 0.0;
	
	
	//constructores
	public Cuenta (int numero, double saldo) {
		Cuenta.numero = numero;
		Cuenta.saldo = saldo;
	}
	
	
	// setters
    public void depositar(int numero_cuenta, double importe) {
        
        Cuenta.numero = numero_cuenta;
        Cuenta.saldo += importe;
    }
    
    public void debitar(int numero_cuenta, double importe) {
        
        Cuenta.numero = numero_cuenta;
        Cuenta.saldo -= importe;
    }
	
	//getters
	
    public static double getSaldo(int numero_cuenta) {
        
        
       return Cuenta.saldo;
    }
	
	
	
}
